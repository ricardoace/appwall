Template.form.events({
    'submit form': function (event) {
        event.preventDefault();
        var imageFile =
            event.currentTarget.children[0].files[0];

        Collections.Images.insert(imageFile, function (error, fileobject) {
            if (error) {

            } else {
                //submit post data to database
                //Author, Message, Created by Date, ImageID 
                //fileobject._id *submit to database*
                $('.grid').masonry('reloadItems');
            }
        });
    }
});